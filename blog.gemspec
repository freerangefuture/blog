$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "gluttonberg/blog/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "gluttonberg-blog"
  s.version     = Gluttonberg::Blog::VERSION
  s.authors     = ["Nick Crowther", "Yuri Tomanek", "Abdul Rauf"]
  s.email       = ["office@freerangefuture.com"]
  s.homepage    = "http://gluttonberg.com"
  s.summary     = "Gluttonberg Blog is a peice of cake"
  s.description = "Create integrated blogs with powerful publishing flows and user management abilities."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_runtime_dependency 'gluttonberg-core', '~> 3.0', '>= 3.0.2'
  s.add_development_dependency 'rspec-rails', '3.1.0'
end
